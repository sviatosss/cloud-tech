# Settings
aws configure

# Creation of bucket
aws s3api create-bucket --bucket sviatosss-bucket --region us-west-2 --create-bucket-configuration LocationConstraint=us-west-2

# Upload files
aws s3 cp Q:\webpage.html s3://sviatosss-bucket

# Enable website
aws s3api put-bucket-website --bucket sviatosss-bucket --website-configuration file://Q://website.json

# Setup bucket policy
aws s3api put-bucket-policy --bucket sviatosss-bucket --policy file://Q://policy.json

aws s3 website s3://sviatosss-bucket/ --index-document file://Q://index.html --error-document file://Q://error.html